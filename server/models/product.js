const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Category = require('./category');
const User = require('./user');

var productSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es necesario'] },
    priceUni: { type: Number, required: [true, 'El precio únitario es necesario'] },
    description: { type: String, required: false },
    avaliable: { type: Boolean, required: true, default: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    del: { type: Boolean, default: false },
});


module.exports = mongoose.model('product', productSchema);