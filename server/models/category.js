const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;
const User = require('./user');

let categorySchema = new Schema({
    description: { type: String, unique: true, required: [true, 'La descripción es necesaria'] },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    del: { type: Boolean, default: false },
});

categorySchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser unico'
});

module.exports = mongoose.model('Category', categorySchema);